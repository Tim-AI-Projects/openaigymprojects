# Helper method for printing
def print_episode_result(episode_nr, cum_reward, episode_steps):
  print("Episode {}: Average reward: {} | Steps: {}".format(episode_nr, cum_reward / episode_nr, episode_steps))  