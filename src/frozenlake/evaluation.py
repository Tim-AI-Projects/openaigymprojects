# Import everything we need
import gym 
from IPython.display import clear_output
import numpy as np
import time
import random

print('Starting Evaluation')

# Setup the environment and print the action- and observation space
env = gym.make("FrozenLake-v1").env

"""Evaluate agent's performance after Q-learning"""
q_table = np.load("src/frozenlake/q_table_frozenlake.npy")
total_episode_steps = 0
episodes = 10

frames = []

# Evaluate the training by only selecting the best possible actions
for episode in range(episodes):
    state = env.reset()
    episode_steps, reward = 0, 0
    
    done = False
    
    while not done:
        action = np.argmax(q_table[state]) # Get the best possible action
        state, reward, done, info = env.step(action)
        episode_steps += 1

        # Put each rendered frame into dict for animation
        frames.append({
            'frame': env.render(mode='ansi'),
            'state': state,
            'action': action,
            'reward': reward,
            'episode': episode,
            'episode_step': episode_steps
            }
        )

        total_episode_steps += episode_steps

        if episode_steps > 100:
            done = True

# Fancy animation of the results of the evaluation
for i, frame in enumerate(frames):
    clear_output(wait=True)
    print(frame['frame'])
    print(f"Step count: {frame['episode_step']}")
    print(f"State: {frame['state']}")
    print(f"Action: {frame['action']}")
    print(f"Reward: {frame['reward']}")
    print(f"Episode: {frame['episode']}")
    time.sleep(.1) # Can also be .1
        

print(f"Results after {episodes} episodes:")
print(f"Average timesteps per episode: {total_episode_steps / episodes}") # lower is better