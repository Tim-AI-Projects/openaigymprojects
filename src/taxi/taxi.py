# Import everything we need
import gym 
from IPython.display import clear_output
import numpy as np
import time
import random

from logger import print_episode_result

# Setup the environment and print the action- and observation space
env = gym.make("Taxi-v3").env
action_space_count = env.action_space.n # Amount of possible actions: Left, Right, Up, Down, Pick up person and Drop off person. So 6
observation_space_count = env.observation_space.n # Total possible states: 5x5x5x4 = 500 5 by 5 grid, passenger can be in 5 different spots (all pick up points and also in the car) and 4 drop off points which makes 500
print("Actions: ", action_space_count)
print("Observations: ", observation_space_count)
episode_count = 10_000

# Create the Q-Table [500, 6]
# The table will be generated with 0 values so for example it could be 
# [0, 0, 0]                                    [-0.41,  -1.68, -0.5]
# [0, 0, 0] and after training it could become [-2.213, -5.2,  -1.2]    
q_table = np.zeros([observation_space_count, action_space_count])

learning_rate = 0.1 # Learning Rate or Alpha, is the extent to which Q-values are being updated in every iteration.

# The discount factor or gamma, determines how much importance we want to give to future rewards. 
# A high value for the discount factor (close to 1) captures the long-term effective award, whereas, 
# a discount factor of 0 makes the AI consider only immediate reward, hence making it greedy.
discount_factor = 0.6 


# There's a tradeoff between exploration (choosing a random action) and exploitation (choosing actions based on already learned Q-values). 
# We want to prevent the action from always taking the same route, and possibly overfitting, this is why the randomness threshold (or Epsilon) is used.
# Instead of just selecting the best learned Q-value action, we'll sometimes favor exploring the action space further. Lower epsilon value results in episodes with more penalties (on average) 
# which is obvious because we are exploring and making random decisions to full in our Q-Table
randomness_threshold = 0.1

print('Start Training')

for episode in range(episode_count):
  state = env.reset() # Reset the environment after every episode
  done = False
  cum_reward = 0 # Keeps track of the total reward for each episode
  episode_steps = 0 # Keeps track of the amount of times the while loop is ran for each episode
  
  # Loop for the current episode
  while not done:
    # Select an action based on a random number between 0 and 1
    if random.uniform(0, 1) < randomness_threshold:
      action = env.action_space.sample() # Explore action space: Get a random action
    else:
      action = np.argmax(q_table[state]) # Exploit learned values: Get the current best possible action

    next_state, reward, done, info = env.step(action) # Apply the selected action to the environment and get the results of the taken action
    old_q = q_table[state, action] # Get the current q value of the state action combination
    next_max = np.max(q_table[next_state]) # Get the max Q value of the next state

    # Magic
    new_q = old_q + learning_rate * (reward + (discount_factor * (next_max - old_q)))


    q_table[state, action] = new_q # Replace the old q with the new calculated q

    cum_reward += reward
    episode_steps += 1
    state = next_state # Before going to the next step, set the new state

  # Lets see the results of the training for every 500 episodes
  if episode % 500 == 0 and episode != 0:
    print_episode_result(episode, cum_reward, episode_steps) # lower episode steps is better and higher cum_reward is better


np.save("src/taxi/q_table_taxi", q_table)
print('Done Training')


# Helper method for printing
def print_episode_result(episode_nr, cum_reward, episode_steps):
  print("Episode {}: Average reward: {} | Steps: {}".format(episode_nr, cum_reward / episode_nr, episode_steps))  